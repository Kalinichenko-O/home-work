//Метод forEach перебирає масив та застосовує функцію callback для кожного елемента 1 раз, який знаходиться в масиві в порядку зростання.

let arr = ['sasha','string',4,5,true,false,{},null,[1,2,3],undefined];
const filteredBy = (arr, type) => arr.filter(item => typeof(item) !== type);

console.log(filteredBy(arr,'string'));