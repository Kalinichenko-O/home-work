// Екранування - це спосіб передачі символів в коді для правильного розпізнання інтерпритатором наприклад, щоб інтерпритатор прочитав одинарну кавичку як текст, а не закінчення рядка. 

function createNewUser() {
    let firstNameValue = prompt('Enter the first name', 'Sasha');
    let lastNameValue = prompt('Enter the last name', 'Kalinichenko');
    let birthdayValue = prompt('Enter the date of birthday','5.10.1991');
    
    let addUser = {
        get firstName() {
            return firstNameValue;
        },
        setFirstName: function(value) {
            firstNameValue = value;
        }, 
        get lastName() {
            return lastNameValue;
        },
        get birthday() {
            return birthdayValue;
        },
        setLastName: function(value) {
            lastNameValue = value;
        }, 
        getLogin: function() {
            return this.firstName[0].toLocaleLowerCase()+this.lastName.toLocaleLowerCase()
        }, 
        getPassword : function() {
            let birthDate = new Date(this.birthday);
            return this.firstName[0].toUpperCase() + this.lastName.toLocaleLowerCase() + birthDate.getFullYear()
        },
        getAge : () => {
            let today = new Date();
            let birthDate = new Date(this.birthday);
            let age = today.getFullYear() - birthDate.getFullYear();
        
            let m = today.getMonth() - birthDate.getMonth();
            let d = today.getDay() - birthDate.getDay();
        
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            if ( age === 0 ) {
                m = 12 + m;
                if (d < 0 || (d === 0 && today.getDate() < birthDate.getDate())) {
                    m--;
                }
            }
        
            return age ? age + ' років' : m + ' місяців';
        }
    }
    return addUser; 
}
let user = createNewUser()
console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());