let $page = $('html, body');
$('a[href*="#"]').click(function () {
  $page.animate(
    {
      scrollTop: $($.attr(this, 'href')).offset().top,
    },
    700
  );
  return false;
});

$('#btn').click(function () {
  $('#section').slideToggle('slide');
});

let btn = $('#button');
$(window).scroll(function () {
  if ($(window).scrollTop() > window.screen.height) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function (e) {
  e.preventDefault();
  $('html, body').animate({ scrollTop: 0 }, '700');
});
