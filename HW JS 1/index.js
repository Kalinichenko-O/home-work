// Цикли потрібні для виконання простих однотипних задач, які потрібно повторювати n-ну кількість разів, щоб не засмічувати код, використовують, цикли які пишуться в декілька рядків, замість багаторазових повторів одного й того ж коду.


let num = Number(prompt('Enter the number'));
while ((!Number.isInteger(num))) {
    console.log(num);
    console.log(Number.isInteger(num));
    num = Number(prompt('Enter the correct number'));
}
console.log(num);
console.log(Number.isInteger(num));
if (num < 5) {
    console.log('Sorry, no numbers');
} else {
    for(let i = 0; i < num; i++) {
        if(i % 5 === 0 && i !== 0){
            console.log('порядок чисел',i);
        }
    }
}

// ------------------------------------------task 2--------------------------------

let m = Number(prompt('Enter the number - m'));
let n = Number(prompt('Enter the number - n'));
while (m > n || n === 1 || n === 0 || m === 1 || m === 0 ) {
    alert('Error');
    m = Number(prompt('Enter the number - m'));
    n = Number(prompt('Enter the number - n'));
} 
for (let i = m; i < n; i++) {
    let simple = true;
    for (let j = 2; j < i; j++) {
        if (i % j === 0) { 
            simple = false; 
            break; 
        }
    }
    if (simple) {
        console.log(i);
    }
}