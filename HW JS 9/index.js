let tab = function() {
    let tab = document.querySelectorAll('.tabs-title'),
    tabContent = document.querySelectorAll('.tab'),
    tabName;

    tab.forEach(item => item.addEventListener('click', selectTab));
    function selectTab() {
        tab.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-name');
        selectTabName(tabName);
    }
    function selectTabName(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active')
        });
    }
}
tab()