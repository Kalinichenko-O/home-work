// Почему для работы с input не рекомендуется использовать события клавиатуры? - Для повноцінної обробки подій пов'язаних з полем для вводу інформації(input) недостатньо використовувати події клавіатури, так як у нас є додаткові події, як події мишки, голосового вводу, розпізнання символів прописом на екрані смартфона, тому для обробки подій інпута використовують спеціальну подію input, за допомогою якого можна відслідковувати будь які дії з полем input.

const BLUE = 'blue';
const BLACK = 'black';

const lowerCase = (item) => item.toLowerCase();

const wrapperButton = document.getElementsByClassName('btn-wrapper')[0].children;

document.addEventListener('keyup',(e) => {
    const key = lowerCase(e.key);

    Array.from(wrapperButton).forEach((item) => {
        if (lowerCase(item.innerHTML) === key) {
            item.style.backgroundColor = BLUE;
        } else {
            item.style.backgroundColor = BLACK;
        }
    })
})





