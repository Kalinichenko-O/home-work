function CSSLoad(file){
    let link = document.createElement("link");
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", file);
    document.getElementsByTagName("head")[0].appendChild(link)
}

const onloadPage = () => {
    const bgColor = localStorage.getItem('bgColor');

    if(bgColor) {
       CSSLoad(`./css/style${bgColor}Theme.css`);
    }     
}

const btn = () => {
    const bgColor = localStorage.getItem('bgColor');

    if(bgColor === 'Grey'){
        localStorage.setItem('bgColor', 'White');
        CSSLoad('./css/styleWhiteTheme.css');
    } else {
        localStorage.setItem('bgColor', 'Grey');
        CSSLoad('./css/styleGreyTheme.css');
    }
}

