const menuBtn = document.querySelector('.menu-btn');
const menuright = document.querySelector('.main-nav');
const menuItem = document.querySelector('.main-nav__link');
let menuOpen = false;
let menuOn = false;
menuBtn.addEventListener('click', () => {
  if (!menuOpen) {
    menuBtn.classList.add('open');
    menuright.style.display = 'block';
    menuOpen = true;
  } else {
    menuBtn.classList.remove('open');
    menuright.style.display = 'none';
    menuOpen = false;
  }
});
menuItem.addEventListener('click', () => {
  if (!menuOn) {
    menuItem.classList.add('active');
    menuItem.style.background = 'black';
    menuOn = true;
  } else {
    menuItem.classList.remove('active');
    menuItem.style.display = 'none';
    menuOn = false;
  }
});
