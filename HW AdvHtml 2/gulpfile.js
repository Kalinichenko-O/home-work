const gulp = require('gulp'),
  clean = require('gulp-clean'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  minifyCSS = require('gulp-clean-css'),
  rename = require('gulp-rename'),
  imagemin = require('gulp-imagemin'),
  minifyjs = require('gulp-js-minify'),
  babel = require('gulp-babel'),
  autoprefixer = require('gulp-autoprefixer');

/*** PATHS ***/
const paths = {
  src: {
    scss: './src/scss/**/*.scss',
    js: './src/js/*.js',
    img: './src/img/**/*',
  },
  dist: {
    css: './dist/css/',
    js: './dist/js/',
    self: './dist/',
    img: './dist/img/',
  },
};

/*** FUNCTIONS ***/
const distImg = () =>
  gulp.src(paths.src.img).pipe(imagemin()).pipe(gulp.dest(paths.dist.img));

const distJS = () =>
  gulp
    .src(paths.src.js)
    .pipe(
      babel({
        presets: ['@babel/env'],
      })
    )
    .pipe(minifyjs())
    .pipe(rename('script.min.js'))
    .pipe(gulp.dest(paths.dist.js))
    .pipe(browserSync.stream());

const distJSDev = () =>
gulp
  .src(paths.src.js)
  .pipe(
    babel({
      presets: ['@babel/env'],
    })
  )
  .pipe(concat("scripts.min.js"))
  .pipe(minifyjs())
  .pipe(gulp.dest(paths.dist.js))
  .pipe(browserSync.stream());

const distCSS = () =>
  gulp
    .src(paths.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest(paths.dist.css))
    .pipe(browserSync.stream());

const distCSSDev = () =>
gulp
  .src(paths.src.scss)
  .pipe(sass().on('error', sass.logError))
  .pipe(minifyCSS())
  .pipe(rename('style.min.css'))
  .pipe(
    autoprefixer({
      browsers: ["last 3 versions"],
      cascade: false,
    })
  )
  .pipe(gulp.dest(paths.dist.css))
  .pipe(browserSync.stream());

const cleandist = () =>
  gulp.src(paths.dist.self, { allowEmpty: true }).pipe(clean());

const watcher = () => {
  browserSync.init({
    server: {
      baseDir: './',
    },
  });

  gulp.watch(paths.src.scss, distCSS).on('change', browserSync.reload);
  gulp.watch(paths.src.js, distJS).on('change', browserSync.reload);
  gulp.watch(paths.src.scss, distCSSDev).on("change", browserSync.reload);
  // gulp.watch(paths.html).on('change', browserSync.reload);
};

/*** TASKS ***/
gulp.task('clean', cleandist);
gulp.task('distCSS', distCSS);
gulp.task('distJS', distJS);
gulp.task('distImg', distImg);

gulp.task('default', gulp.series(cleandist, distCSS, distJS, distImg, watcher));

gulp.task('dev', gulp.series(cleandist, gulp.series(distCSSDev, distJSDev), watcher));
gulp.task('build',gulp.series(cleandist, gulp.series(distCSS, distJS, distImg)));
