// Опишите своими словами, что такое метод обьекта - Метод об'єкта - це функція всередині об'єкта яка являється властивістю самого об'єкта, методи дозволяють взаємодіяти з об'єктами. 

function createNewUser() {
    let firstNameValue = prompt('Enter the first name', 'Sasha');
    let lastNameValue = prompt('Enter the last name', 'Kalinichenko');

    let addUser = {
        get firstName() {
            return firstNameValue;
        },
        setFirstName: function(value) {
            firstNameValue = value;
        }, 
        get lastName() {
            return lastNameValue;
        },
        setLastName: function(value) {
            lastNameValue = value;
        }, 
        getLogin: function() {
           return this.firstName[0].toLocaleLowerCase()+this.lastName.toLocaleLowerCase()
        } 
    }
    return addUser;
}
let user = createNewUser()
console.log(user.getLogin());
