class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary * 3;
  }
}

const prog1 = new Programmer('Dan', 25, 1000, 'En,Ua');
const prog2 = new Programmer('Sasha', 30, 3000, 'En,Ua,It');
console.log(prog1);
console.log(prog2);
console.log(prog1.salary);
console.log(prog2.salary);
