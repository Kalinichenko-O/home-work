const block = document.getElementById('block');
const loader = document.querySelector('.middle');
const fetchResult = fetch('https://ajax.test-danit.com/api/swapi/films');
fetchResult
  .then(response => {
    if (response.ok) {
      return response.json();
    }
    throw new Error('Couldn`t catch response');
  })
  .then(films => {
    films.forEach(film => {
      console.log(film);
      block.insertAdjacentHTML(
        'beforeend',
        `<ul><li>Назва фільму ${film.name}, номер епізоду${film.episodeId}, короткий опис  ${film.openingCrawl}</li></ul> `
      );

      film.characters.forEach(character => {
        const characterData = fetch(character);
        characterData
          .then(resp => {
            if (resp.ok) {
              loader.style.display = 'none';
              return resp.json();
            }
            throw new Error('Error');
          })
          .then(respData => {
            console.log(respData.name);
            block.insertAdjacentHTML('beforeend', `<p>${respData.name}</p>`);
          });
      });
    });
  })
  .catch(error => {
    console.log(error);
  });
