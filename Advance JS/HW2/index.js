// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
// Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
  {
    author: 'Скотт Бэккер',
    name: 'Тьма, что приходит прежде',
    price: 70,
  },
  {
    author: 'Скотт Бэккер',
    name: 'Воин-пророк',
  },
  {
    name: 'Тысячекратная мысль',
    price: 70,
  },
  {
    author: 'Скотт Бэккер',
    name: 'Нечестивый Консульт',
    price: 70,
  },
  {
    author: 'Дарья Донцова',
    name: 'Детектив на диете',
    price: 40,
  },
  {
    author: 'Дарья Донцова',
    name: 'Дед Снегур и Морозочка',
  },
];

let list = books.map(({author,name,price}) => {
    try {
        if(author && name && price){
            return `
            <li> Author : ${author}</li>
            <li> Name : ${name}</li>
            <li> Price : ${price}</li>
            `
        } else if (!author) {
            throw new Error('Відсутній автор книги')
        } else if (!name) {
            throw new Error('Відсутня назва книги')
        } else if (!price){
            throw new Error('Відсутня ціна книги')
        }
    }
    catch (error) {
        console.error(`Error: ${error.message}`)
    }
})

function arrayToList(array, block = document.body){
    block.insertAdjacentHTML('beforeend', `<ul>${array.join(' ')}</ul>`)
}
arrayToList(list,document.querySelector('#root'))

