// При открытии страницы, необходимо получить с сервера список всех пользователей и общий список публикаций. Для этого нужно отправить GET запрос на следующие два адреса:

// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts

// После загрузки всех пользователей и их публикаций, необоходимо отобразить все публикации на странице.
// Каждая публикация должна быть отображена в виде карточки (пример: https://prnt.sc/q2em0x), и включать заголовок, текст, а также имя, фамилию и имейл пользователя, который ее разместил.
// На каждой карточке должна присутствовать иконка или кнопка, которая позволит удалить данную карточку со страницы. При клике на нее необходимо отправить DELETE запрос по адресу https://ajax.test-danit.com/api/json/posts/${postId}. После получения подтверждения с сервера (запрос прошел успешно), карточку можно удалить со страницы, используя JavaScript.
// Более детальную информацию по использованию каждого из этих указанных выше API можно найти здесь.
// Данный сервер является тестовым. После перезагрузки страницы все изменения, которые отправлялись на сервер, не будут там сохранены. Это нормально, все так и должно работать.
// Карточки обязательно должны быть реализованы в виде ES6 классов. Для этого необходимо создать класс Card. При необходимости, вы можете добавлять также другие классы.

// Необязательное задание продвинутой сложности

// Пока с сервера при открытии страницы загружается информация, показывать анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.
// Добавить вверху страницы кнопку Добавить публикацию. При нажатии на кнопку, открывать модальное окно, в котором пользователь сможет ввести заголовок и текст публикации. После создания публикации данные о ней необходимо отправить в POST запросе по адресу: https://ajax.test-danit.com/api/json/posts. Новая публикация должна быть добавлена вверху страницы (сортировка в обратном хронологическом порядке). В качестве автора можно присвоить публикации пользователя с id: 1.
// Добавить функционал (иконку) для редактирования содержимого карточки. После редактирования карточки для подтверждения изменений необходимо отправить PUT запрос по адресу https://ajax.test-danit.com/api/json/posts/${postId}.

const BASE_URL = 'https://ajax.test-danit.com/api/json';

class CallApi {
  fetch(url) {
    return fetch(url, { method: 'GET' })
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(response.status);
      })
      .catch(err => {
        throw new Error(err);
      });
  }
  delete(id) {
    return fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
      method: 'DELETE',
    })
      .then(response => {
        if (response.ok) {
          const post = document.getElementById(`button-remove-${id}`);
          const btn = document.getElementById(id);
          post.remove();
          btn.remove();
        } else {
          throw new Error(response.status);
        }
      })
      .catch(err => {
        throw new Error(err);
      });
  }
}

const callApi = new CallApi();

class JoinPostToUsers {
  joinPostToUsers(usersArray, postsArray) {
    return usersArray.map(user => {
      return {
        ...user,
        posts: postsArray.filter(post => post.userId === user.id),
      };
    });
  }
}
const joinPTU = new JoinPostToUsers();

class Render {
  constructor() {
    this.sectionPost = document.getElementById('section');
    this.users = callApi.fetch(`${BASE_URL}/users`);
    this.posts = callApi.fetch(`${BASE_URL}/posts`);
    this.wrapper = document.createElement('div');
  }

  render() {
    this.users
      .then(data => {
        data.forEach(({ id, name, email }) => {
          this.sectionPost.innerHTML += `
        <div><p class="author">Author : ${name}</p>
        <p class="email">email : ${email}</p>
        <div class="text" id="posts-${id}"></div>
        </div>
        `;
        });
      })
      .then(() => {
        this.posts.then(data => {
          data.forEach(item => {
            const userPosts = document.getElementById(`posts-${item.userId}`);
            const button = document.createElement('button');
            button.setAttribute('id', item.id);
            button.classList.add('btn-remove');
            button.innerText = 'X';
            button.addEventListener(
              'click',
              this.deletePost.bind(this, item.id)
            );
            userPosts.appendChild(button);
            userPosts.insertAdjacentHTML(
              'beforeend',
              `<div id="button-remove-${item.id}"><h2 class="title">Title : ${item.title}</h2>
            <p class="post"> ${item.body}</p></div> `
            );
          });
        });
      });
  }

  deletePost(id) {
    callApi.delete(id);
  }
}
const users = new Render();
users.render();
