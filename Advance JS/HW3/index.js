// #1
// const clients1 = [
//   'Гилберт',
//   'Сальваторе',
//   'Пирс',
//   'Соммерс',
//   'Форбс',
//   'Донован',
//   'Беннет',
// ];
// const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон'];
// const fullClients = [...clients1, ...clients2];
// const uniqClients = fullClients.reduce((uniq, item) => {
//   return uniq.includes(item) ? uniq : [...uniq, item];
// }, [])
// console.log(uniqClients);

// #2
// Создайте на его основе массив charactersShortInfo, состоящий из объектов, в которых есть только 3 поля - name, lastName и age.

// const characters = [
//   {
//     name: 'Елена',
//     lastName: 'Гилберт',
//     age: 17,
//     gender: 'woman',
//     status: 'human',
//   },
//   {
//     name: 'Кэролайн',
//     lastName: 'Форбс',
//     age: 17,
//     gender: 'woman',
//     status: 'human',
//   },
//   {
//     name: 'Аларик',
//     lastName: 'Зальцман',
//     age: 31,
//     gender: 'man',
//     status: 'human',
//   },
//   {
//     name: 'Дэймон',
//     lastName: 'Сальваторе',
//     age: 156,
//     gender: 'man',
//     status: 'vampire',
//   },
//   {
//     name: 'Ребекка',
//     lastName: 'Майклсон',
//     age: 1089,
//     gender: 'woman',
//     status: 'vempire',
//   },
//   {
//     name: 'Клаус',
//     lastName: 'Майклсон',
//     age: 1093,
//     gender: 'man',
//     status: 'vampire',
//   },
// ];

// let charactersShortInfo = characters.map(({gender, status, ...rest}) => rest);
// console.log(charactersShortInfo);


// #3
// const user1 = {
//   name: 'John',
//   years: 30,
// }
// const { name, years, isAdmin = false } = user1;
// console.log(name)
// console.log(years)
// console.log(isAdmin)

// #4
// Детективное агентство несколько лет собирает информацию о возможной личности Сатоши Накамото. Вся информация, собранная в конкретном году, хранится в отдельном объекте. Всего таких объектов три - satoshi2018, satoshi2019, satoshi2020.
// Чтобы составить полную картину и профиль, вам необходимо объединить данные из этих трех объектов в один объект - fullProfile.
// Учтите, что некоторые поля в объектах могут повторяться. В таком случае в результирующем объекте должно сохраниться значение, которое было получено позже (например, значение из 2020 более приоритетное по сравнению с 2019).
// Напишите код, который составит полное досье о возможной личности Сатоши Накамото. Изменять объекты satoshi2018, satoshi2019, satoshi2020 нельзя.

// const satoshi2020 = {
//   name: 'Nick',
//   surname: 'Sabo',
//   age: 51,
//   country: 'Japan',
//   birth: '1979-08-21',
//   location: {
//     lat: 38.869422,
//     lng: 139.876632,
//   },
// };

// const satoshi2019 = {
//   name: 'Dorian',
//   surname: 'Nakamoto',
//   age: 44,
//   hidden: true,
//   country: 'USA',
//   wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//   browser: 'Chrome',
// };

// const satoshi2018 = {
//   name: 'Satoshi',
//   surname: 'Nakamoto',
//   technology: 'Bitcoin',
//   country: 'Japan',
//   browser: 'Tor',
//   birth: '1975-04-05',
// };

// const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020}
// console.log(fullProfile)

// #5
// Дан массив книг. Вам нужно добавить в него еще одну книгу, не изменяя существующий массив (в результате операции должен быть создан новый массив).
// const books = [{
//   name: 'Harry Potter',
//   author: 'J.K. Rowling'
// }, {
//   name: 'Lord of the rings',
//   author: 'J.R.R. Tolkien'
// }, {
//   name: 'The witcher',
//   author: 'Andrzej Sapkowski'
// }];

// const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
// }
// const newBook = [...books, bookToAdd];
// console.log(newBook);

// #6
// Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть создан новый объект, который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.

// const employee = {
//   name: 'Vitalii',
//   surname: 'Klichko'
// }
// const user = {...employee, age:44, salary: 1000}
// console.log(user)


// #7
// Дополните код так, чтоб он был рабочим
// const array = ['value', () => 'showValue'];
// const [value, showValue] = array

// alert(value); // должно быть выведено 'value'
// alert(showValue());  // должно быть выведено 'showValue'

