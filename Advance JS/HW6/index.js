const BASE_URL = 'https://api.ipify.org/?format=json';
const IP_URL = 'http://ip-api.com/json';
const button = document.getElementById('btn');
button.addEventListener('click', () => {
  getIp();
});
const getIp = async () => {
  try {
    const response = await fetch(`${BASE_URL}`);
    if (response.ok) {
      const { ip } = await response.json();
      try {
        const resp = await fetch(
          `${IP_URL}/${ip}?fields=status,continent,country,regionName,city,district`
        );
        if (resp.ok) {
          const findIp = await resp.json();
          const { continent, country, regionName, city, district } = findIp;
          const block = document.getElementById('div');
          block.innerHTML = `<p>Continent: ${continent},
           country: ${country}, 
            region: ${regionName},
            City: ${city}, 
            District: ${district}</p>`;
        } else {
          throw new Error('error');
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      throw new Error('error');
    }
  } catch (error) {
    console.log(error);
  }
};
