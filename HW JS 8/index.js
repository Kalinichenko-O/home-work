// Обробник подій - це частина коду, яка відповідає за виконання заданих команд при настанні певних подій.

const field = document.getElementById('price');
field.className = 'price';
const container = document.getElementById('container');
function getPrice() {
    const wrapper = document.createElement('div');
    field.addEventListener('focus',(e) => {
        e.target.style.border = '4px solid green';
    },)
    
    field.addEventListener('blur',(e) => {
        const btn = document.createElement('button');
        btn.textContent = 'X';
        let price = e.target.value;
        const span = document.createElement('span');
        span.style.display = 'block';
        
        btn.addEventListener('click',() => {
            document.getElementById('price').value = '';
            let sp = document.getElementsByTagName('span')[0];
            sp.remove();
        },)
        
        if (price <= 0) {
            field.style.border = '4px solid red';
            span.innerText = 'Please enter correct price.';
            span.append(btn);
            container.append(span);
            
        } else {
            span.innerText = `Текущая цена: ${price}`;
            span.append(btn);
            container.prepend(span);            
        }
        },)    
    field.append(wrapper)
    

}
getPrice()