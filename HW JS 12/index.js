// Опишите своими словами разницу между функциями setTimeout() и setInterval(). Функція setTimeout() викликає функцію, яка передається в параметр один раз через заданий інтервал, а функція setInterval() запускається періодично через заданий інтервал.

// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему? Функція визовиться з нульовою затримкою, тільки після того як виконається поточний код. 
// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен? СlearInterval() потрібен для того щоб зупинити функцію setInterval(), поки не буде заданий СlearInterval() функція буде залишатись в пам'яті, а також будуть існувати зовнішні змінні, які можуть займати досить багато пам'яті. 

const wrapper = document.getElementsByClassName('images-wrapper')[0];
let count = 1;
let timeId = null;

const buttonPause = document.createElement('button');
buttonPause.textContent = 'Pause';
document.body.prepend(buttonPause);
const buttonResume = document.createElement('button');
buttonResume.textContent = 'Resume';
document.body.prepend(buttonResume);

function counter(contin) {
    if (contin) {
        const prevImg = document.getElementsByClassName('image-to-show')[0];
        if (prevImg) {
            prevImg.remove()
        }
        const img = document.createElement('img');
        img.classList.add('image-to-show');
        img.src = `./img/${count}.${count !== 4 ? 'jpg' : 'png'}`;
        wrapper.append(img);
        count++;
        timeId = setTimeout(function(){counter(contin)}, 3000);
        if (count === 5){
            count = 1;
        }
    }
    else {
        clearTimeout(timeId);
    }
}
buttonPause.addEventListener('click', () => {
    counter(false);
})

buttonResume.addEventListener('click', () => {
    setTimeout(function(){counter(true)}, 3000);
})

counter(true);


