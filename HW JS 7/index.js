// Document Object Model (DOM) - це незалежний від мови програмування інтерфейс, який дозволяє програмам та скриптам отримувати доступ до елементів HTML та XML, змінювати їх наповнення, структуру та оформлення.

let arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
let list = document.getElementById('list');

const renderArray = (arr, node = document.body) => {
	 
	let buildTree = item =>
    Array.isArray(item)
    ? ` <ul>${item.map(buildTree).join('')}</ul>`
    : ` <li>${item}</li>`;

	node.innerHTML = buildTree(arr);
    setTimeout(() => node.remove(), 5000);    

}

renderArray(arr);