import { BrowserRouter as Router } from 'react-router-dom';

import Routes from './Routes/Routes';
import Header from './components/Header/Header.module';

import styles from './App.module.scss';

const App = () => {
  return (
    <Router>
      <div className={styles.App}>
        <Header title='Store' />
        <Routes />
      </div>
    </Router>
  );
};

export default App;
