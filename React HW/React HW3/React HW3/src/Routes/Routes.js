import { Switch, Route, Redirect } from 'react-router-dom';

import HomePage from '../pages/HomePage/HomePage';
import FavouritePage from '../pages/FavouritePage/FavouritePage';
import CartPage from '../pages/CartPage/CartPage';

const Routes = () => {
  return (
    <Switch>
      <Route exact path='/' component={HomePage} />
      <Route exact path='/favourite' component={FavouritePage} />
      <Route exact path='/cart' component={CartPage} />
      <Redirect to='/' />
    </Switch>
  );
};

export default Routes;
