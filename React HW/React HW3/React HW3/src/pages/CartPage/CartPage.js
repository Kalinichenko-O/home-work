import { Fragment, useState, useEffect } from 'react';

import Item from '../../components/item/Item';
import Modal from '../../components/Modal/Modal';

import styles from './cartPage.module.scss';

const CartPage = () => {
  const [actualModal, setActualModal] = useState(null);

  const [cartData, setCartData] = useState([]);

  useEffect(() => {
    const cartStorage = localStorage.getItem('cartData');
    if (cartStorage) {
      setCartData(JSON.parse(cartStorage));
    }
  }, []);

  const handleDelete = elem => {
    const newData = cartData.filter(item => elem.name !== item.name);
    setCartData(newData);
    localStorage.setItem('cartData', JSON.stringify(newData));
  };

  const handleModal = name => setActualModal(name);
  const handleConfirm = item => {
    handleDelete(item);
    handleModal(null);
  };

  return (
    <section className={styles.cartPageSection}>
      <>{cartData.length !== 0 ? 
      (cartData.map(item => (
        <Fragment key={item.article}>
          <Item
            handleDelete={() => handleModal(item.name)}
            key={item.article}
            {...item}
          />

          {actualModal === item.name && (
            <Modal
              modalBackground={styles.modalBackgroundSecond}
              backgroundColorBtn='grey'
              buttonClose
              handleClose={() => handleModal(null)}
              header='Delete'
              text={item.name}
              handleConfirm={() => {
                handleConfirm(item);
              }}
            />
          )}
        </Fragment>
      ))) : <p>No items in cart</p>}
      </>
    </section>
  );
};

export default CartPage;
