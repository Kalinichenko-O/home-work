import { useState, useEffect } from 'react';

import ItemsContainer from '../../components/ItemsContainer/ItemsContainer';

const changeData = (data, localData) => {
  return data.map(elem => {
    const itemFromStorage = localData.find(item => item.name === elem.name);
    if (itemFromStorage && elem.name === itemFromStorage.name) {
      return { ...elem, favourite: itemFromStorage.favourite };
    } else {
      return { ...elem, favourite: false };
    }
  });
};

const HomePage = () => {
  const [actualModal, setActualModal] = useState(null);
  const [data, setData] = useState([]);
  const [cartData, setCartData] = useState([]);

  useEffect(() => {
    const tempLocalData = localStorage.getItem('favouriteItems');
    const localData = tempLocalData === null ? [] : JSON.parse(tempLocalData);

    const fetchData = async () => {
      const fetchData = await fetch('./data.json');
      const { data } = await fetchData.json();

      const modifyData = changeData(data, localData);
      setData(modifyData);
    };
    fetchData();
  }, []);

  const handleModal = name => setActualModal(name);

  const onFavourite = item => {
    const newData = data.map(elem => {
      if (elem.name === item.name) {
        return {
          ...elem,
          favourite: !elem.favourite,
        };
      } else {
        return elem;
      }
    });
    const favouriteItems = newData.filter(elem => elem.favourite);
    setData(newData);
    localStorage.setItem('favouriteItems', JSON.stringify(favouriteItems));
  };

  const addItems = item => {
    const dataStorage = localStorage.getItem('cartData');
    const cartDataStorage = dataStorage ? JSON.parse(dataStorage) : [];
    const newCardData = [...cartDataStorage, item];
    setCartData(newCardData);
    localStorage.setItem('cartData', JSON.stringify(newCardData));
  };
  return (
    <ItemsContainer
      onFavourite={onFavourite}
      addItems={addItems}
      handleModal={handleModal}
      actualModal={actualModal}
      data={data}
    />
  );
};

export default HomePage;
