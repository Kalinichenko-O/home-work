import { useState, useEffect } from 'react';

import Item from '../../components/item/Item';

import styles from './favouritePage.module.scss';

const FavouritePage = props => {
  const [favouriteProd, setFavouriteProd] = useState([]);

  useEffect(() => {
    const dataStorage = localStorage.getItem('favouriteItems');
    if (dataStorage) {
      setFavouriteProd(JSON.parse(dataStorage));
    }
  }, []);
  const onFavourite = elem => {
    const newData = favouriteProd.filter(item => item.name !== elem.name);
    setFavouriteProd(newData);
    localStorage.setItem('favouriteItems', JSON.stringify(newData));
  };
  return (
    <section className={styles.favouriteSection}>
      <>{favouriteProd.length !== 0 ? (favouriteProd.map(item => (
        <Item onFavourite={() => onFavourite(item)} key={item.name} {...item} />
      ))) : <p>No selected items</p>}
      </>
    </section>
  );
};

export default FavouritePage;
