import React from 'react';
import PropTypes from 'prop-types';

import icon from '../../assets/iconClose.png';
import styles from './CloseIcon.module.scss';

const CloseIcon = (props) => {
  const { onClick } = props;
  return (
    <img
        onClick={onClick}
        className={styles.closeIcon}
        src={icon}
        alt='icon'
    />
    
  );
};

CloseIcon.propTypes = {
    onClick: PropTypes.func
}

export default CloseIcon;
