import PropTypes from 'prop-types';

import styles from './button.module.scss';

const Button = props => {
  const { backgroundColor, text, onClick } = props;

  return (
    <button
      type='button'
      onClick={onClick}
      className={styles.btn}
      style={{ backgroundColor: backgroundColor }}
      // {...props}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
  props: PropTypes.object,
};

Button.defaultProps = {
  backgroundColor: '',
  text: '',
  onClick: () => {},
  props: {},
};

export default Button;
