import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ReactComponent as StartSVG } from '../../assets/star-plus.svg';
import { ReactComponent as StartSVGSelected } from '../../assets/star-remove.svg';
import CloseIcon from '../CloseIcon/CloseIcon';

import styles from './item.module.scss';

const Item = props => {
  const {
    name,
    price,
    url,
    article,
    color,
    handleModal,
    onFavourite,
    favourite,
    handleDelete,
  } = props;
  const { pathname } = useLocation();

  return (
    <div className={styles.wrapper}>
      {handleDelete ? <CloseIcon onClick={handleDelete} /> : null}
      <div className={styles.favourites} onClick={onFavourite}>
        <>
          {pathname !== '/cart' ? (
            favourite ? (
              <StartSVGSelected className={styles.starSelected} />
            ) : (
              <StartSVG className={styles.star} />
            )
          ) : null}
        </>
      </div>
      <img src={url} alt={name} />
      <p>{name}</p>
      <p>{article}</p>
      <p>{color}</p>
      <span>{price}$</span>
      {handleModal ? (
        <button
          type='button'
          onClick={handleModal}
          className={styles.buttonToCart}
        >
          Add to cart
        </button>
      ) : null}
    </div>
  );
};

Item.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  handleModal: PropTypes.func,
  onFavourite: PropTypes.func,
  favourite: PropTypes.bool,
};

Item.defaultProps = {
  name: '',
  price: '',
  url: '',
  article: '',
  color: '',
  onFavourite: () => {},
  favourite: false,
};

export default Item;
