import PropTypes from 'prop-types';

import Button from '../Button/Button';
import CloseIcon from '../CloseIcon/CloseIcon';

import styles from './modal.module.scss';

const Modal = (props) => {
  const {
    text,
    header,
    buttonClose,
    handleClose,
    modalBackground,
    backgroundColorBtn,
    handleConfirm,
  } = props;
  const handleModalClickWrapper = e => {
    if (e.currentTarget === e.target) {
      handleClose();
    }
  };
  return (
    <div className={styles.modalBackground} onClick={handleModalClickWrapper}>
      <div className={`${styles.modal} ${modalBackground}`}>
        <div className={`${styles.modalHeader} ${modalBackground}`}>
          <h2 className={styles.modalHeaderTitle}>{header}</h2>
          {buttonClose && <CloseIcon onClick={handleClose} />}
        </div>
        <div className={styles.modalBody}>
          <p className={styles.modalBodyText}>{text}</p>
        </div>
        <div className={styles.modalFooter}>
          <Button
            backgroundColor={backgroundColorBtn}
            onClick={handleConfirm}
            text='ok'
          />
          <Button
            backgroundColor={backgroundColorBtn}
            onClick={handleClose}
            text='cancel'
          />
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  buttonClose: PropTypes.bool,
  text: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  handleClose: PropTypes.func,
  backgroundColorBtn: PropTypes.string,
  modalBackground: PropTypes.string,
  handleConfirm: PropTypes.func,
};

Modal.defaultProps = {
  header: '',
  buttonClose: true,
  text: '',
  handleClose: () => {},
  backgroundColorBtn: '',
  modalBackground: '',
  handleConfirm: '',
};

export default Modal;
