import { NavLink } from 'react-router-dom';

import styles from './header.module.scss';

const Header = props => {
  const { title } = props;
  return (
    <div className='header'>
      <h1>{title}</h1>
      <nav className={styles.navBar}>
        <ul className={styles.navigate}>
          <li className={styles.navigateLink}>
            <NavLink className={styles.navigateItem} exact to='/'>
              Home
            </NavLink>
          </li>
          <li className={styles.navigateLink}>
            <NavLink className={styles.navigateItem} exact to='/favourite'>
              Favourite
            </NavLink>
          </li>
          <li className={styles.navigateLink}>
            <NavLink className={styles.navigateItem} exact to='/cart'>
              Cart
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Header;
