import { Component } from 'react';

import ItemsContainer from './components/ItemsContainer/ItemsContainer';

import styles from './App.module.scss';

const changeData = data => data.map(item => ({...item, favourite: false }));

class App extends Component {
  state = {
    actualModal: null,
    data: [],
    cartData: [],
    onFavourite: false,
  };

  async componentDidMount() {
    const fetchData = await fetch('./data.json');
    const { data } = await fetchData.json();
    
    const modifyData = changeData(data);
    this.setState({ data: modifyData });
  }

  render() {
    const handleModal = name => this.setState({ actualModal: name });

    const onFavourite = item => {
      const newData = this.state.data.map(elem => {
        if (elem.name === item.name) {
          return {
            ...elem,
            favourite: !elem.favourite,
          };
        } else {
          return elem;
        }
      });
      const favouriteItems = newData.filter(elem => elem.favourite);
      
      this.setState({ data: newData });
      localStorage.setItem('favouriteItems', JSON.stringify(favouriteItems));
    };

    const addItems = item => {
      const newCardData = [...this.state.cartData, item];

      this.setState({ cartData: newCardData });
      localStorage.setItem('cartData', JSON.stringify(newCardData));
    };

    return (
      <div className={styles.App}>
        <ItemsContainer
          onFavourite={onFavourite}
          addItems={addItems}
          handleModal={handleModal}
          actualModal={this.state.actualModal}
          data={this.state.data}
        />
      </div>
    );
  }
}

export default App;
