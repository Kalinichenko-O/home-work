import { Component } from 'react';
import PropTypes from 'prop-types';

import icon from '../../assets/iconClose.png';
import styles from './CloseIcon.module.scss';

class CloseIcon extends Component {
  render() {
    const { onClick } = this.props;
    return (
      <img
        onClick={onClick}
        className={styles.closeIcon}
        src={icon}
        alt='icon'
      />
    );
  }
}
CloseIcon.propTypes = {
    onClick: PropTypes.func
}

export default CloseIcon;
