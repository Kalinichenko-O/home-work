import { Component, Fragment } from 'react';

import Item from '../item/Item';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';

import styles from './itemsContainer.module.scss';

class ItemsContainer extends Component {
  render() {
    const { data, handleModal, actualModal, addItems, onFavourite } =
      this.props;

    const handleConfirm = item => {
        addItems(item);
        handleModal(null);
    };

    return (
      <section className={styles.root}>
        <h1>Products</h1>
        <div className={styles.container}>
          {data.map(item => (
            <Fragment key={item.article}>
              <Item
                onFavourite={() => onFavourite(item)}
                handleModal={() => handleModal(item.name)}
                actualModal={actualModal}
                {...item}
              />

              {actualModal === item.name && (
                <Modal
                  modalBackground={styles.modalBackgroundSecond}
                  backgroundColorBtn='grey'
                  buttonClose
                  handleClose={() => handleModal(null)}
                  header='Add to Cart'
                  text={item.name}
                  handleConfirm={() => {
                    handleConfirm(item);
                  }}
                />
              )}
            </Fragment>
          ))}
        </div>
      </section>
    );
  }
}

ItemsContainer.propTypes = {
  data: PropTypes.array,
  handleModal: PropTypes.func,
  actualModal: PropTypes.string,
  addItems: PropTypes.func,
  onFavourite: PropTypes.func,
};
ItemsContainer.defaultProps = {
    data: [],
    handleModal: ()=> {},
    actualModal: '',
    addItems: () => {},
    onFavourite: () => {},
}

export default ItemsContainer;
