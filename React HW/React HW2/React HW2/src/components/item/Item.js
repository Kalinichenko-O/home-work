import { Component } from 'react';

import PropTypes from 'prop-types';
import { ReactComponent as StartSVG } from '../../assets/star-plus.svg';
import { ReactComponent as StartSVGSelected } from '../../assets/star-remove.svg';

import styles from './item.module.scss';

class Item extends Component {
  render() {
    const {
      name,
      price,
      url,
      article,
      color,
      handleModal,
      onFavourite,
      favourite,
    } = this.props;
    return (
      <div className={styles.wrapper}>
        <div className={styles.favourites} onClick={onFavourite}>
          {favourite ? (
            <StartSVGSelected className={styles.starSelected} />
          ) : (
            <StartSVG className={styles.star} />
          )}
        </div>
        <img src={url} alt={name} />
        <p>{name}</p>
        <p>{article}</p>
        <p>{color}</p>
        <span>{price}$</span>
        <button
          type='button'
          onClick={handleModal}
          className={styles.buttonToCart}
        >
          Add to cart
        </button>
      </div>
    );
  }
}
Item.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  handleModal: PropTypes.func,
  onFavourite: PropTypes.func,
  favourite: PropTypes.bool,
};
Item.defaultProps = {
  name: '',
  price: '',
  url: '',
  article: '',
  color: '',
  handleModal: () => {},
  onFavourite: () => {},
  favourite: false
};

export default Item;
