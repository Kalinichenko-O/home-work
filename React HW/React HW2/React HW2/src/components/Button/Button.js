import { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './button.module.scss';

class Button extends Component {
  render() {
    const { backgroundColor, text, onClick, ...props } = this.props;

    return (
      <button
        type='button'
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
        className={styles.btn}
        {...props}
      >
        {text}
      </button>
    );
  }
}
Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
  props: PropTypes.object,
};
Button.defaultProps = {
  backgroundColor: '',
  text: '',
  onClick: () => {},
  props: {},
};

export default Button;
