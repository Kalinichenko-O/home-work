import { Component } from 'react';

import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

import styles from './App.module.scss';

class App extends Component {
  state = {
    actualModal: null,
  };
  render() {
    const handleModal = name => this.setState({ actualModal: name });

    return (
      <div className={styles.App}>
        <Button text='First modal' onClick={() => handleModal('firstModal')} />
        <Button text='Sec modal' onClick={() => handleModal('secondModal')} />
        {this.state.actualModal === 'firstModal' && (
          <Modal
            modalBackground={styles.modalBackgroundFirst}
            backgroundColorBtn='#b3382c'
            buttonClose
            handleClose={() => handleModal(null)}
            header='first modal'
            text='first modal text'
          />
        )}
        {this.state.actualModal === 'secondModal' && (
          <Modal
            modalBackground={styles.modalBackgroundSecond}
            backgroundColorBtn='#c42415'
            buttonClose
            handleClose={() => handleModal(null)}
            header='second modal'
            text='second modal text'
          />
        )}
      </div>
    );
  }
}

export default App;
// прибрати зайві скобки
// всі компоненти повинні прийняти всі пропси
// найти іконку покласти в папку assets
// імпорти
// добавить proptypes
