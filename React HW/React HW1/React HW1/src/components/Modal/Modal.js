import { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/Button';
import CloseIcon from '../CloseIcon/CloseIcon';

import styles from './modal.module.scss';

class Modal extends Component {
  render() {
    const {
      text,
      header,
      buttonClose,
      handleClose,
      modalBackground,
      backgroundColorBtn,
    } = this.props;

    const handleModalClickWrapper = e => {
      if (e.currentTarget === e.target) {
        handleClose();
      }
    };

    return (
      <div className={styles.modalBackground} onClick={handleModalClickWrapper}>
        <div className={`${styles.modal} ${modalBackground}`}>
          <div className={`${styles.modalHeader} ${modalBackground}`}>
            <h2 className={styles.modalHeaderTitle}>{header}</h2>
            {buttonClose && <CloseIcon onClick={handleClose} />}
          </div>
          <div className={styles.modalBody}>
            <p className={styles.modalBodyText}>{text}</p>
          </div>
          <div className={styles.modalFooter}>
            <Button
              backgroundColor={backgroundColorBtn}
              onClick={() => console.log('ok')}
              text='ok'
            />
            <Button
              backgroundColor={backgroundColorBtn}
              onClick={handleClose}
              text='cancel'
            />
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  buttonClose: PropTypes.bool,
  text: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  handleClose: PropTypes.func,
  backgroundColorBtn: PropTypes.string,
  modalBackground: PropTypes.string,
};

Modal.defaultProps = {
  header: '',
  buttonClose: true,
  text: '',
  handleClose: () => {},
  backgroundColorBtn: 'red',
  modalBackground: 'orange',
};

export default Modal;
