const passwordInput = document.getElementById('password').childNodes[1];
const passwordIconEye = document.getElementById('password').childNodes[3];
const passwordIconEyeSlash = document.getElementById('password').childNodes[5];

const confirmPasswordInput = document.getElementById('confirm-password').childNodes[1];
const confirmPasswordIconEye = document.getElementById('confirm-password').childNodes[3];
const confirmPasswordIconEyeSlash = document.getElementById('confirm-password').childNodes[5];
const btn = document.getElementById('btn')

const passwordInputValue = document.getElementById('input-password');
const confirmPasswordInputValue = document.getElementById('input-confirm-password');

let valuePassword = '';
let valuePasswordConfirm = '';

passwordInputValue.addEventListener('keyup', (e) => {
    valuePassword = e.target.value;
});

confirmPasswordInputValue.addEventListener('keyup', (e) => {
    valuePasswordConfirm = e.target.value;
});

btn.addEventListener('click', () => {
     if (valuePassword !== valuePasswordConfirm) {
        const errorDiv = document.createElement('div');
        errorDiv.innerHTML = 'Нужно ввести одинаковые значения';
        errorDiv.style.color = 'red';
        errorDiv.id = 'error-div';

        const confirmPassword = document.getElementById('confirm-password');
        confirmPassword.append(errorDiv);        
    } else {        
        const errorDiv = document.getElementById('error-div');
        if(errorDiv) {
            errorDiv.remove();        
        }
        setTimeout(() =>{
            alert('You are welcome');
        },0)
    }
}),

passwordIconEye.addEventListener('click', () => {
    passwordInput.type = 'text';
    passwordIconEye.style.display = 'none';
    passwordIconEyeSlash.style.display = 'block';  
});

passwordIconEyeSlash.addEventListener('click', () => {
    passwordInput.type = 'password';
    passwordIconEye.style.display = 'block';
    passwordIconEyeSlash.style.display = 'none';  
});

confirmPasswordIconEye.addEventListener('click', () => {
    confirmPasswordInput.type = 'text';
    confirmPasswordIconEye.style.display = 'none';
    confirmPasswordIconEyeSlash.style.display = 'block';  
});

confirmPasswordIconEyeSlash.addEventListener('click', () => {
    confirmPasswordInput.type = 'password';
    confirmPasswordIconEye.style.display = 'block';
    confirmPasswordIconEyeSlash.style.display = 'none';  
});



